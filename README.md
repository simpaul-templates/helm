# Helm

Realizar o deploy de aplicações empacotadas pelo [Helm](https://helm.sh/) e entregues no [AWS EKS](https://aws.amazon.com/pt/eks/).
É necessario a criação de nova branch para novos projetos.

## Requisitos

- AWS EKS v1.19.13 ou mais recente
- helm 3 

## Adicionando novo chart

A customização dos templates do chart vai de acordo com as dependencias de cada aplicação.

## Adicionar o repositorio no helm:
```
helm repo add --username <username> --password <access_token> project-1 https://gitlab.example.com/api/v4/projects/31363610/packages/helm/<channel>
```
```
<username>: o nome de usuário do GitLab ou o nome de usuário do token de implantação.
<access_token>: o token de acesso pessoal ou o token de implantação. Faz necessesario a criação do token por meio de api.
<project_id>: o ID do projeto .
<channel>: o nome do canal (usamos o stable).
```
## Atualizar o repositorio do helm:
```
helm repo update
```
## Listar repositorio

Confirme o repositorio na lista:
```
helm repo ls
```
## Deploy

Após ter feito a instação e atualização do repositorio, faça:
```
helm install <nomeaplicação> simpaul/<nomeaplicação>
```
## Desinstalar uma aplicação
```
helm uninstall <nomeaplicação>
```
## Documentação Gitlab

Passa a passo seguindo a documentação abaixo:

https://docs.gitlab.com/ee/user/packages/helm_repository/